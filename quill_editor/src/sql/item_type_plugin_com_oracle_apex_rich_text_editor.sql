set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050000 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2013.01.01'
,p_release=>'5.0.4.00.12'
,p_default_workspace_id=>66550650266685578
,p_default_application_id=>200
,p_default_owner=>'DSO'
);
end;
/
prompt --application/ui_types
begin
null;
end;
/
prompt --application/shared_components/plugins/item_type/com_oracle_apex_rich_text_editor
begin
wwv_flow_api.create_plugin(
 p_id=>wwv_flow_api.id(87902035304556045)
,p_plugin_type=>'ITEM TYPE'
,p_name=>'COM.ORACLE.APEX.RICH_TEXT_EDITOR'
,p_display_name=>'Rich Text Editor Quill'
,p_supported_ui_types=>'DESKTOP'
,p_javascript_file_urls=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'https://cdn.quilljs.com/1.3.4/quill.min.js',
''))
,p_css_file_urls=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'https://cdn.quilljs.com/1.3.4/quill.snow.css',
'#PLUGIN_FILES#plugin/app.min.css'))
,p_plsql_code=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'function render_rich_text_editor (',
'    p_item                in apex_plugin.t_page_item,',
'    p_plugin              in apex_plugin.t_plugin,',
'    p_value               in varchar2,',
'    p_is_readonly         in boolean,',
'    p_is_printer_friendly in boolean )',
'    return apex_plugin.t_page_item_render_result',
'is',
'    -- It''s better to have named variables instead of using the generic ones,',
'    -- makes the code more readable',
'    l_placeholder apex_application_page_items.attribute_01%type := nvl(p_item.attribute_01, '' '');',
'    l_toolbar_group_1 apex_application_page_items.attribute_02%type := nvl(p_item.attribute_02, '''');',
'    l_toolbar_group_2 apex_application_page_items.attribute_03%type := nvl(p_item.attribute_03, '''');',
'    l_label_dropdown  apex_application_page_items.attribute_04%type := p_item.attribute_04;',
'    l_label_values    apex_application_page_items.attribute_05%type := nvl(p_item.attribute_05, '''');',
'    l_word_limit      apex_application_page_items.attribute_06%type := nvl(p_item.attribute_06, ''N'');',
'    l_max_word_count  apex_application_page_items.attribute_07%type := nvl(p_item.attribute_07, 0);',
'',
'    l_name        varchar2(30);',
'    l_result      apex_plugin.t_page_item_render_result;',
'    l_html        varchar2(32767);',
'begin',
'    if p_is_readonly or p_is_printer_friendly then',
'        -- emit hidden field if necessary',
'        apex_plugin_util.print_hidden_if_readonly (',
'            p_item_name           => p_item.name,',
'            p_value               => p_value,',
'            p_is_readonly         => p_is_readonly,',
'            p_is_printer_friendly => p_is_printer_friendly );',
'        -- emit display span with the value',
'        apex_plugin_util.print_display_only (',
'            p_item_name        => p_item.name,',
'            p_display_value    => p_value,',
'            p_show_line_breaks => false,',
'            p_escape           => true,',
'            p_attributes       => p_item.element_attributes );',
'    else',
'        -- Because the page item saves state, we have to call get_input_name_for_page_item',
'        -- which generates the internal hidden p_arg_names field. It will also return the',
'        -- HTML field name which we have to use when we render the HTML input field.',
'        l_name := apex_plugin.get_input_name_for_page_item(false);',
'        l_html := p_value;',
'        ',
'        -- Getting the value from the editor-container via apex.item() or :P1_ITEM returns null because ',
'        -- it is the container element. Because of that a hidden input will hold the html content for the APEX item',
'        sys.htp.p(''<div class="editor-container" style="background-color:#f2f2f2;min-height:300px"></div>'');',
'        sys.htp.p(''<div class="editor-counter">',
'                      <p class="character-count"></p>',
'                  </div>'');',
'        sys.htp.p(''<input id="'' || p_item.name || ''" ',
'                             type="text"',
'                             name="''|| l_name || ''"',
'                             style="display:none;"/>'');',
'                             ',
'',
'        -- Register the javascript library the plug-in uses.',
'        apex_javascript.add_library (',
'            p_name      => ''plugin/ES6_Utility'',',
'            p_directory => p_plugin.file_prefix,',
'            p_version   => null );     ',
'            ',
'   ',
'        apex_javascript.add_library (',
'            p_name      => ''plugin/app.min'',',
'            p_directory => p_plugin.file_prefix,',
'            p_version   => null );  ',
'            ',
'        -- Create variables ',
'        -- quill is the rich text editor',
'        -- htmlContent is the html that is provided by the source ',
'        -- htmlContainer is the input that holds the html content for the APEX item',
'        --     the html ',
'        apex_javascript.add_inline_code (',
'            p_code => ''var quill = "";',
'                       var Block = Quill.import("blots/block");        // REMOVE to return to <p><br></p> as default separator',
'                       Block.tagName = "DIV";',
'                       Quill.register(Block, true);',
'                       var haveRemoved = false;',
'                       var htmlContent = "";',
'                       var htmlContainer ="";',
'                       var textReplacements = ""; ',
'                       var textTooltips = [];',
'                       var maxCount = '' || l_max_word_count || '';',
'                       ',
'                      // Key binding events',
'                      var bindings = {',
'                          delete: {                ',
'                            key: 46,',
'                            handler: function(range, context) {',
'                                var rightChar = quill.getText(range.index, 1);',
'                                var format = quill.getFormat(range.index, 1);',
'            ',
'                                // Use default behavior if no BOLD # is found',
'                                if (rightChar !== "#") { //} || !format.bold) {',
'                                    haveRemoved = true;',
'                                    return true;',
'                                }',
'                            }',
'                          },',
'                          backspace: {',
'                            key: 8,',
'                            handler: function(range, context) {',
'                                var leftChar = quill.getText(range.index - 1, 1);',
'                                var format = quill.getFormat(range.index - 1, 1);',
'            ',
'                                // Use default behavior if no BOLD # is found',
'                                if (leftChar !== "#") { //} || !format.bold) {',
'                                    haveRemoved = true;',
'                                    return true;',
'                                } ',
'                            }',
'                          },',
'                          paste: {',
'                                key: 86, //"v"',
'                                ctrlKey: true,',
'                                handler: function(range, context) {',
'                                    return true;',
'                                }',
'                          }',
'                      };''',
'        );',
'        ',
'        -- Initialize the editor, html (from the query), and the container',
'        apex_javascript.add_onload_code (',
'            p_code => ''// Get the chosen toolbar options',
'                       // A checkbox provides a colon ":" delimited string of the options selected',
'                       var labelToolbar = ("'' || l_label_dropdown || ''" === "N" ) ? "" : [{ "dropdownContainer": [] }] ;',
'                       var toolbarGroup1 = "''|| l_toolbar_group_1 ||''".split(":");',
'                       var toolbarGroup2 = "''|| l_toolbar_group_2 ||''".split(":");',
'                       var labelMap = [];',
'                       var counter =  ("'' || l_word_limit || ''" === "Y") ?  {',
'                                                                        container: ".editor-counter",',
'                                                                        editor: ".ql-editor",',
'                                                                        counter: "p.character-count",',
'                                                                        unit: "character",',
'                                                                        maxCount: '' || l_max_word_count || ''',
'                                                                    } : "";',
'                       // Handle null cases',
'                       toolbarGroup1 = toolbarGroup1[0] === "" ? "" : toolbarGroup1;',
'                       toolbarGroup2 = toolbarGroup2[0] === "" ? "" : toolbarGroup2;',
'',
'                       // Create an array of toolbars',
'                       var toolbars = [].concat(_toConsumableArray(labelToolbar), _toConsumableArray(toolbarGroup1), _toConsumableArray(toolbarGroup2));',
'                       ',
'                       /* ES6 Not compatible with IE',
'                           let toolbars = [...toolbarGroup1, ...toolbarGroup2];',
'                       */',
'                       var labelValues = {'' || l_label_values || ''};',
'            ',
'                       Object.keys(labelValues).forEach(function(key) {',
'                            key, labelValues[key]',
'                            textReplacements += (key) + ":";',
'                            textTooltips.push(labelValues[key]);',
'                       });',
'            ',
'                       // Remove the trailing ":"',
'                       textReplacements = textReplacements.slice(0,-1);',
'            ',
'                       // Create a label map, so that only labels specified will be highlighted',
'                       var textRepl2 = textReplacements.split(":");',
'                       for (let textReplacement of textRepl2) {',
'                            var numValue = 0;',
'                            for(let i = 0; i < textReplacement.length; i++) {',
'                                numValue += textReplacement.charCodeAt(i);',
'                            }',
'                            labelMap.push(numValue);',
'                       }',
'            ',
'                       // Setup the quill editor',
'                       quill = new Quill(".editor-container", {',
'                          modules: {',
'                            keyboard: {',
'                                bindings: bindings',
'                            },',
'                            clipboard: {',
'                                matchVisual: false                        // Prevents quill from adding extra newlines',
'                            },',
'                            toolbar: [',
'                              //[{ "dropdownContainer": [] }],            // Necessary placeholder dropdown that will be replaced (custom dropdowns are not supported)',
'                              //[...toolbars],                            // Get all of the toolbars defined',
'                              [].concat(_toConsumableArray(toolbars))     // Get all of the toolbars defined',
'                            ],',
'                            replacement_dropdown: {                       // Replacement Dropdown module that customizes the dropdown with our content',
'                                container: ".editor-container",',
'                                content: "''||''",',
'                                textReplacementList: textReplacements,',
'                                textTooltipList: textTooltips',
'                            },',
'                            text_replacement: {                           // Text Replacement Module treats text replacements as a single unit rather than',
'                                delimiter: "#",                            // a series of characters',
'                                labelMap: labelMap',
'                            },',
'                            counter',
'                          },',
'                          placeholder: "''|| l_placeholder || ''", ',
'                          theme: "snow"',
'                        });',
'                        htmlContainer = apex.jQuery("#''|| p_item.name ||''");                    // Get a hold of the html container',
'                        htmlContent =''|| '''''''' || TRIM(''"'' from                                        -- Workaround for es6 template literals ',
'                                                      REPLACE(                                        -- Get rid of leading/ending quotes',
'                                                          REPLACE(',
'                                                                    REPLACE(p_value,'''''''',''&#39''),     -- Get rid of apostrophes',
'                                                          chr(10),''''),                                -- Get rid of vertical tabs',
'                                                      chr(13),'''')                                     -- Get rid of newline',
'                                                  ) || '''''''' ||'';                                      // Need to remove single quotes (workaround for ES6 template literals)    ',
'                        console.log(htmlContent);',
'                        apex.jQuery(".ql-toolbar").css("backgroundColor","#fff");               // Change the background color of the toolbar',
'                        ',
'                        htmlContainer.on("change", function() {',
'                            htmlContent = apex.item("'' || p_item.name || ''").getValue();',
'                            quill.clipboard.dangerouslyPasteHTML(htmlContent);     // Populate the editor',
'                            quill.history.clear();                                 // Prevent an undo that removes all of the text',
'                        });',
'            ',
'                        ''',
'        );',
'        ',
'        -- Populate the editor and update the APEX html container on text change',
'        apex_javascript.add_onload_code (',
'            p_code => ''quill.clipboard.dangerouslyPasteHTML(htmlContent);     // Populate the editor',
'                       quill.history.clear();                                 // Prevent an undo that removes all of the text',
'                       htmlContainer.val(htmlContent);                        // Populate the APEX item container',
'            ',
'                       quill.on("text-change", function() {',
'                           console.log("text change",quill.getLength());',
'                           var text = quill.root.innerHTML;                   // Get the innerHTML',
'                           htmlContainer.val(text);                           // Update the APEX item container with the edited content       ',
'                       });''',
'        );',
'        ',
'        l_result.is_navigable := true;',
'    end if;',
'    return l_result;',
'end render_rich_text_editor;'))
,p_render_function=>'render_rich_text_editor'
,p_standard_attributes=>'VISIBLE:READONLY:SOURCE:ELEMENT:ENCRYPT'
,p_substitute_attributes=>true
,p_subscribe_plugin_settings=>true
,p_help_text=>'Displays a text area with (currently) limited text formatting options (Toolbar customization will be added). End users can enhance the content displayed in a similar fashion to using a word processor, such as Microsoft Word.'
,p_version_identifier=>'1.0'
,p_about_url=>'https://quilljs.com/'
,p_plugin_comment=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'BSD 3-Clause License',
'',
'Copyright (c) 2014, Jason Chen',
'Copyright (c) 2013, salesforce.com',
'All rights reserved.',
'',
'Redistribution and use in source and binary forms, with or without',
'modification, are permitted provided that the following conditions',
'are met:',
'',
'1. Redistributions of source code must retain the above copyright',
'notice, this list of conditions and the following disclaimer.',
'',
'2. Redistributions in binary form must reproduce the above copyright',
'notice, this list of conditions and the following disclaimer in the',
'documentation and/or other materials provided with the distribution.',
'',
'3. Neither the name of the copyright holder nor the names of its',
'contributors may be used to endorse or promote products derived from',
'this software without specific prior written permission.',
'',
'THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS',
'IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED',
'TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A',
'PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT',
'HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,',
'SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT',
'LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,',
'DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY',
'THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT',
'(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE',
'OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.'))
,p_files_version=>378
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(87929336506424315)
,p_plugin_id=>wwv_flow_api.id(87902035304556045)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>1
,p_display_sequence=>10
,p_prompt=>'Placeholder'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_default_value=>'Create a template..'
,p_display_length=>1
,p_max_length=>100
,p_is_translatable=>false
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(87975013054579141)
,p_plugin_id=>wwv_flow_api.id(87902035304556045)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>2
,p_display_sequence=>20
,p_prompt=>'Settings 1'
,p_attribute_type=>'CHECKBOXES'
,p_is_required=>false
,p_is_translatable=>false
,p_lov_type=>'STATIC'
);
wwv_flow_api.create_plugin_attr_value(
 p_id=>wwv_flow_api.id(87975937593581884)
,p_plugin_attribute_id=>wwv_flow_api.id(87975013054579141)
,p_display_sequence=>10
,p_display_value=>'Bold'
,p_return_value=>'bold'
,p_help_text=>'Option to bold text.'
);
wwv_flow_api.create_plugin_attr_value(
 p_id=>wwv_flow_api.id(87976845193584414)
,p_plugin_attribute_id=>wwv_flow_api.id(87975013054579141)
,p_display_sequence=>20
,p_display_value=>'Italic'
,p_return_value=>'italic'
,p_help_text=>'Option to italicize text.'
);
wwv_flow_api.create_plugin_attr_value(
 p_id=>wwv_flow_api.id(87977218944587981)
,p_plugin_attribute_id=>wwv_flow_api.id(87975013054579141)
,p_display_sequence=>30
,p_display_value=>'Underline'
,p_return_value=>'underline'
,p_help_text=>'Option to underline text.'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(87984241359867001)
,p_plugin_id=>wwv_flow_api.id(87902035304556045)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>3
,p_display_sequence=>30
,p_prompt=>'Settings 2'
,p_attribute_type=>'CHECKBOXES'
,p_is_required=>false
,p_is_translatable=>false
,p_lov_type=>'STATIC'
);
wwv_flow_api.create_plugin_attr_value(
 p_id=>wwv_flow_api.id(87985141271869648)
,p_plugin_attribute_id=>wwv_flow_api.id(87984241359867001)
,p_display_sequence=>10
,p_display_value=>'Link'
,p_return_value=>'link'
,p_help_text=>'Option to enable hyperlink inserts.'
);
wwv_flow_api.create_plugin_attr_value(
 p_id=>wwv_flow_api.id(87985490146871572)
,p_plugin_attribute_id=>wwv_flow_api.id(87984241359867001)
,p_display_sequence=>20
,p_display_value=>'Image'
,p_return_value=>'image'
,p_help_text=>'Option to enable image inserts.'
);
wwv_flow_api.create_plugin_attr_value(
 p_id=>wwv_flow_api.id(87985941652874437)
,p_plugin_attribute_id=>wwv_flow_api.id(87984241359867001)
,p_display_sequence=>30
,p_display_value=>'Code Block'
,p_return_value=>'code-block'
,p_help_text=>'Option to add a code block section.'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(71229846069353162)
,p_plugin_id=>wwv_flow_api.id(87902035304556045)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>4
,p_display_sequence=>40
,p_prompt=>'Label Dropdown'
,p_attribute_type=>'CHECKBOX'
,p_is_required=>false
,p_default_value=>'N'
,p_is_translatable=>false
,p_help_text=>'Adds a dropdown toolbar with predefined labels to insert into the text editor.'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(71231890892380531)
,p_plugin_id=>wwv_flow_api.id(87902035304556045)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>5
,p_display_sequence=>50
,p_prompt=>'Label Values'
,p_attribute_type=>'TEXTAREA'
,p_is_required=>true
,p_is_translatable=>false
,p_depending_on_attribute_id=>wwv_flow_api.id(71229846069353162)
,p_depending_on_condition_type=>'EQUALS'
,p_depending_on_expression=>'Y'
,p_help_text=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'<h3>Proper Label Syntax</h3>',
'<strong>#PROPER_LABEL#</strong> does',
'<strong>NOT</strong> have whitespace and is surrounded by ''<strong>#</strong>''. A label with lowercase or numbers will <strong>NOT</strong> be recognized.',
'<br> Without using this syntax, the text editor will <strong>NOT</strong> recognize a label as a single unit.<br>',
'<br>',
'<h3>Proper Tooltip Syntax</h3>',
'A tooltip consists of a string value<br>',
'<br>',
'<h3>Usage</h3>',
'Define labels and their respective tooltips using javascript object declaration syntax.<br>',
'Use double quotes (<strong>""</strong>) to avoid having to escape special characters.',
'<br> "key" : "value",',
'<br> "key2" : "value 2"',
'<br>',
'<br>',
'<strong>The following:</strong>',
'<br> "#LABEL_ONE#":"Label 1 tooltip",',
'<br> "#LABEL_TWO#":"Label 2 tooltip"',
'<br>',
'<br> Will result in:',
'<br>',
'<strong>#LABEL_ONE#</strong> with tooltip',
'<strong>Label 1 tooltip</strong>',
'<br>',
'<strong>#LABEL_ONE#</strong> with tooltip',
'<strong>Label 2 tooltip</strong>'))
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(72766994275562876)
,p_plugin_id=>wwv_flow_api.id(87902035304556045)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>6
,p_display_sequence=>60
,p_prompt=>'Character Limit'
,p_attribute_type=>'CHECKBOX'
,p_is_required=>false
,p_default_value=>'N'
,p_is_translatable=>false
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(72767540633565152)
,p_plugin_id=>wwv_flow_api.id(87902035304556045)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>7
,p_display_sequence=>70
,p_prompt=>'Max Character Count'
,p_attribute_type=>'NUMBER'
,p_is_required=>true
,p_max_length=>1000
,p_unit=>'words'
,p_is_translatable=>false
,p_depending_on_attribute_id=>wwv_flow_api.id(72766994275562876)
,p_depending_on_condition_type=>'EQUALS'
,p_depending_on_expression=>'Y'
);
end;
/
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
wwv_flow_api.g_varchar2_table(1) := '76617220436F756E7465723D2F2A2A2040636C617373202A2F66756E6374696F6E28297B66756E6374696F6E207428742C65297B76617220693D746869733B746869732E7175696C6C3D742C746869732E6F7074696F6E733D652C746869732E65646974';
wwv_flow_api.g_varchar2_table(2) := '6F723D242822222B652E656469746F72292C746869732E636F6E7461696E65723D242822222B652E636F6E7461696E6572292C746869732E636F756E7465723D652E636F756E7465722C746869732E6D6178436F756E743D652E6D6178436F756E742C74';
wwv_flow_api.g_varchar2_table(3) := '6869732E6E6F6E546578744B6579733D5B382C34362C33352C33362C33372C33382C33392C34305D2C2428652E656469746F72292E6F6E28226B6579646F776E222C66756E6374696F6E28297B72657475726E20692E68616E646C654B6579446F776E28';
wwv_flow_api.g_varchar2_table(4) := '6576656E74297D292C2428652E656469746F72292E6F6E28227061737465222C66756E6374696F6E28297B72657475726E20692E68616E646C655061737465286576656E74297D2C746869732E757064617465436F756E7465722829292C746869732E71';
wwv_flow_api.g_varchar2_table(5) := '75696C6C2E6F6E2822746578742D6368616E6765222C66756E6374696F6E28297B72657475726E20692E757064617465436F756E74657228297D297D72657475726E20742E70726F746F747970652E63616C63756C6174653D66756E6374696F6E28297B';
wwv_flow_api.g_varchar2_table(6) := '76617220743D746869732E6765744C656E67746828293B72657475726E22776F7264223D3D3D746869732E6F7074696F6E732E756E69743F303C28743D742E7472696D2829292E6C656E6774683F742E73706C6974282F5C732B2F292E6C656E6774683A';
wwv_flow_api.g_varchar2_table(7) := '303A747D2C742E70726F746F747970652E68616E646C654B6579446F776E3D66756E6374696F6E2874297B746869732E6C696D69744D657428292626746869732E6973546578744B657928742E6B6579436F6465292626742E70726576656E7444656661';
wwv_flow_api.g_varchar2_table(8) := '756C7428297D2C742E70726F746F747970652E757064617465436F756E7465723D66756E6374696F6E28297B76617220743D746869732E63616C63756C61746528292C653D746869732E6F7074696F6E732E756E69743B746869732E6C696D69744D6574';
wwv_flow_api.g_varchar2_table(9) := '28293F282428746869732E6F7074696F6E732E656469746F72292E616464436C617373282272656422292C2428746869732E6F7074696F6E732E636F756E746572292E616464436C61737328227265642229293A282428746869732E6F7074696F6E732E';
wwv_flow_api.g_varchar2_table(10) := '656469746F72292E72656D6F7665436C617373282272656422292C2428746869732E6F7074696F6E732E636F756E746572292E72656D6F7665436C61737328227265642229292C31213D3D74262628652B3D227322292C746869732E636F6E7461696E65';
wwv_flow_api.g_varchar2_table(11) := '722E6368696C6472656E28746869732E636F756E746572292E7465787428742B2220222B652B22206F6620222B746869732E6D6178436F756E742B2220636861726163746572732C20222B28746869732E6D6178436F756E742D74292B222072656D6169';
wwv_flow_api.g_varchar2_table(12) := '6E696E6722297D2C742E70726F746F747970652E68616E646C6550617374653D66756E6374696F6E2874297B76617220653D742E636C6970626F617264446174612E6765744461746128227465787422292C693D746869732E6765744C656E6774682829';
wwv_flow_api.g_varchar2_table(13) := '2C6E3D692B652E6C656E6774682C6F3D6E3B6966287175696C6C2E666F63757328292C693C746869732E6D6178436F756E74297B6966286E3E746869732E6D6178436F756E74297B76617220723D652E737562737472696E6728302C746869732E6D6178';
wwv_flow_api.g_varchar2_table(14) := '436F756E742D69292C6C3D28746869732E6D6178436F756E742C7175696C6C2E67657453656C656374696F6E2829293B7175696C6C2E696E7365727454657874286C2E696E6465782C722C2273696C656E7422292C6F3D692B722E6C656E6774682C742E';
wwv_flow_api.g_varchar2_table(15) := '70726576656E7444656661756C7428297D7D656C7365206F3D746869732E6D6178436F756E742C742E70726576656E7444656661756C7428293B7175696C6C2E73657453656C656374696F6E286F2C312C2273696C656E7422297D2C742E70726F746F74';
wwv_flow_api.g_varchar2_table(16) := '7970652E6C696D69744D65743D66756E6374696F6E28297B72657475726E20746869732E6765744C656E67746828293D3D3D746869732E6D6178436F756E747D2C742E70726F746F747970652E6973546578744B65793D66756E6374696F6E2874297B72';
wwv_flow_api.g_varchar2_table(17) := '657475726E2D313D3D3D746869732E6E6F6E546578744B6579732E696E6465784F662874297D2C742E70726F746F747970652E6765744C656E6774683D66756E6374696F6E28297B72657475726E20746869732E656469746F722E7465787428292E6C65';
wwv_flow_api.g_varchar2_table(18) := '6E6774687D2C747D28293B5175696C6C2E726567697374657228226D6F64756C65732F636F756E746572222C436F756E746572293B766172205265706C6163656D656E7444726F70646F776E3D66756E6374696F6E28297B66756E6374696F6E20742874';
wwv_flow_api.g_varchar2_table(19) := '2C65297B746869732E7175696C6C3D742C746869732E6F7074696F6E733D652C746869732E636F6E7461696E65723D242822222B652E636F6E7461696E6572292C746869732E746578745265706C6163656D656E744C6973743D652E746578745265706C';
wwv_flow_api.g_varchar2_table(20) := '6163656D656E744C6973742C746869732E746578745265706C6163656D656E74733D746869732E746578745265706C6163656D656E744C6973742E73706C697428223A22292C746869732E74657874546F6F6C7469704C6973743D652E74657874546F6F';
wwv_flow_api.g_varchar2_table(21) := '6C7469704C6973742C746869732E696E697469616C697A6544726F70646F776E28292C746869732E696E697469616C697A6544726F70646F776E546F6F6C7469707328293B766172206E3D746869732E7175696C6C3B2428222E716C2D64726F70646F77';
wwv_flow_api.g_varchar2_table(22) := '6E436F6E7461696E6572202E716C2D7069636B65722D6974656D22292E6F6E2822636C69636B222C66756E6374696F6E28297B76617220743D6E2E67657453656C656374696F6E282130292C653D742E696E6465782C693D242874686973292E61747472';
wwv_flow_api.g_varchar2_table(23) := '2822646174612D636F6E74656E7422293B6E2E6765744C656E67746828292B692E6C656E6774683C3D6D6178436F756E742626286E2E64656C6574655465787428742E696E6465782C742E6C656E677468292C6E2E636C6970626F6172642E64616E6765';
wwv_flow_api.g_varchar2_table(24) := '726F75736C79506173746548544D4C28652C22222B692C2261706922292C2428222E716C2D64726F70646F776E436F6E7461696E65722E716C2D7069636B657222292E746F67676C65436C6173732822716C2D657870616E64656422292C6E2E73657453';
wwv_flow_api.g_varchar2_table(25) := '656C656374696F6E28652B692E6C656E67746829297D297D72657475726E20742E70726F746F747970652E696E697469616C697A6544726F70646F776E3D66756E6374696F6E28297B76617220693D746869733B746869732E7365745069636B65724C61';
wwv_flow_api.g_varchar2_table(26) := '62656C282253656C6563742061206C6162656C22292C746869732E746578745265706C6163656D656E74732E666F72456163682866756E6374696F6E28742C65297B2428222E716C2D64726F70646F776E436F6E7461696E65722E716C2D7069636B6572';
wwv_flow_api.g_varchar2_table(27) := '203E202E716C2D7069636B65722D6F7074696F6E7322292E617070656E6428692E6765745069636B65724974656D28652B3129292C2428222E716C2D64726F70646F776E436F6E7461696E6572202E716C2D7069636B65722D6974656D5B646174612D76';
wwv_flow_api.g_varchar2_table(28) := '616C75653D27222B28652B31292B22275D22292E617474722822646174612D636F6E74656E74222C74297D292C746869732E636F6E7461696E65722E636C6F7365737428222E742D466F726D2D696E707574436F6E7461696E657222292E63737328226F';
wwv_flow_api.g_varchar2_table(29) := '766572666C6F77222C2276697369626C6522292C746869732E636F6E7461696E65722E636C6F7365737428222E72656C2D636F6C22292E63737328226F766572666C6F77222C2276697369626C6522297D2C742E70726F746F747970652E696E69746961';
wwv_flow_api.g_varchar2_table(30) := '6C697A6544726F70646F776E546F6F6C746970733D66756E6374696F6E28297B746869732E74657874546F6F6C7469704C6973742E666F72456163682866756E6374696F6E28742C65297B2428222E716C2D64726F70646F776E436F6E7461696E657220';
wwv_flow_api.g_varchar2_table(31) := '2E716C2D7069636B65722D6974656D5B646174612D76616C75653D27222B28652B31292B22275D22292E617474722822646174612D746F6F6C746970222C74297D297D2C742E70726F746F747970652E7365745069636B65724C6162656C3D66756E6374';
wwv_flow_api.g_varchar2_table(32) := '696F6E2874297B2428222E716C2D64726F70646F776E436F6E7461696E6572202E716C2D7069636B65722D6C6162656C22292E617474722822646174612D636F6E74656E74222C74297D2C742E70726F746F747970652E6765745069636B65724974656D';
wwv_flow_api.g_varchar2_table(33) := '3D66756E6374696F6E2874297B72657475726E273C7370616E20636C6173733D22716C2D7069636B65722D6974656D2220646174612D76616C75653D22272B742B27223E3C2F7370616E3E277D2C747D28293B5175696C6C2E726567697374657228226D';
wwv_flow_api.g_varchar2_table(34) := '6F64756C65732F7265706C6163656D656E745F64726F70646F776E222C5265706C6163656D656E7444726F70646F776E293B76617220546578745265706C6163656D656E743D66756E6374696F6E28297B66756E6374696F6E207428742C65297B746869';
wwv_flow_api.g_varchar2_table(35) := '732E7175696C6C3D742C746869732E6F7074696F6E733D652C746869732E64656C696D697465723D652E64656C696D697465722C746869732E6C6162656C4D61703D652E6C6162656C4D61702C746869732E53504143455F4B45593D2220222C74686973';
wwv_flow_api.g_varchar2_table(36) := '2E4E45575F4C494E453D225C6E222C746869732E75706461746528297D72657475726E20742E70726F746F747970652E7570646174653D66756E6374696F6E28297B766172206F3D746869733B2428222E716C2D656469746F7222292E6F6E28226B6579';
wwv_flow_api.g_varchar2_table(37) := '646F776E222C66756E6374696F6E2874297B76617220653D22222C693D742E6B6579436F64657C7C742E63686172436F64653B38213D6926263436213D697C7C28653D7061727365496E74286F2E676574437572736F72496E646578282929292C383D3D';
wwv_flow_api.g_varchar2_table(38) := '692626286F2E6E6F4368617252656D6F7665642829262628652D3D312C6F2E736561726368416E6452656D6F766528652C6F2C3029292C6F2E7265736574496E64696361746F722829292C34363D3D692626286F2E6E6F4368617252656D6F7665642829';
wwv_flow_api.g_varchar2_table(39) := '262628652B3D312C6F2E736561726368416E6452656D6F766528652C6F2C3129292C6F2E7265736574496E64696361746F722829297D292C746869732E7175696C6C2E6F6E282273656C656374696F6E2D6368616E6765222C66756E6374696F6E287429';
wwv_flow_api.g_varchar2_table(40) := '7B666F722876617220653D5B5D2C693D313B693C617267756D656E74732E6C656E6774683B692B2B29655B692D315D3D617267756D656E74735B695D3B69662874297B766172206E3D742E696E6465783B6F2E7175696C6C2E67657453656C656374696F';
wwv_flow_api.g_varchar2_table(41) := '6E282130292C6F2E736561726368286E2C6F297D6F2E7265736574496E64696361746F7228297D297D2C742E70726F746F747970652E7365617263683D66756E6374696F6E28742C652C69297B696628652E637572736F724C6F636174696F6E56616C69';
wwv_flow_api.g_varchar2_table(42) := '64287429297B766172206E3D652E676574446972656374696F6E2874292C6F3D652E6765744C6162656C426F756E6473286E2C74293B6966286F2626652E73657453656C656374696F6E286F292C2279223D3D3D692972657475726E206F7D7D2C742E70';
wwv_flow_api.g_varchar2_table(43) := '726F746F747970652E6E6F4368617252656D6F7665643D66756E6374696F6E28297B72657475726E216861766552656D6F7665647D2C742E70726F746F747970652E7265736574496E64696361746F723D66756E6374696F6E28297B6861766552656D6F';
wwv_flow_api.g_varchar2_table(44) := '7665643D21317D2C742E70726F746F747970652E736561726368416E6452656D6F76653D66756E6374696F6E28742C652C69297B766172206E3D652E73656172636828742C652C227922293B6E3F652E72656D6F766553656C656374696F6E286E293A65';
wwv_flow_api.g_varchar2_table(45) := '2E72656D6F766553656C656374696F6E287B7374617274496E6465783A742D692C6C656E6774683A317D297D2C742E70726F746F747970652E676574437572736F72496E6465783D66756E6374696F6E28297B72657475726E207175696C6C2E67657453';
wwv_flow_api.g_varchar2_table(46) := '656C656374696F6E282130292C7175696C6C2E67657453656C656374696F6E28292E696E6465787D2C742E70726F746F747970652E637572736F724C6F636174696F6E56616C69643D66756E6374696F6E2874297B76617220653D7175696C6C2E676574';
wwv_flow_api.g_varchar2_table(47) := '5465787428742C31292C693D7175696C6C2E6765745465787428742D312C31293B72657475726E212821657C7C21692926262865213D3D746869732E53504143455F4B4559262665213D3D746869732E4E45575F4C494E45262669213D3D746869732E53';
wwv_flow_api.g_varchar2_table(48) := '504143455F4B4559262669213D3D746869732E4E45575F4C494E4526262865213D3D746869732E64656C696D697465727C7C69213D3D746869732E64656C696D6974657229297D2C742E70726F746F747970652E676574446972656374696F6E3D66756E';
wwv_flow_api.g_varchar2_table(49) := '6374696F6E2874297B72657475726E207175696C6C2E6765745465787428742C31293D3D3D746869732E64656C696D697465723F2D313A317D2C742E70726F746F747970652E6765744C6162656C426F756E64733D66756E6374696F6E28742C65297B66';
wwv_flow_api.g_varchar2_table(50) := '6F722876617220693D302C6E3D21312C6F3D21312C723D302C6C3D652B3D312A742C733D303B746869732E697356616C69642865292626216E3B297175696C6C2E6765745465787428652C31293D3D3D746869732E64656C696D69746572262628723D65';
wwv_flow_api.g_varchar2_table(51) := '2C742A3D2D312C6E3D2130292C692B3D7175696C6C2E6765745465787428652C31292E63686172436F646541742830292C653D746869732E757064617465496E64657828652C74292C732B2B3B696628653D746869732E757064617465496E646578286C';
wwv_flow_api.g_varchar2_table(52) := '2C74292C216E2972657475726E20303B666F72283B746869732E697356616C69642865292626216F3B297175696C6C2E6765745465787428652C31293D3D3D746869732E64656C696D697465722626286C3D652C6F3D2130292C692B3D7175696C6C2E67';
wwv_flow_api.g_varchar2_table(53) := '65745465787428652C31292E63686172436F646541742830292C653D746869732E757064617465496E64657828652C74292C732B2B3B72657475726E20746869732E697356616C69644C6162656C28692926266F3F7B7374617274496E6465783A6C3D6C';
wwv_flow_api.g_varchar2_table(54) := '3C723F6C3A722C6C656E6774683A737D3A307D2C742E70726F746F747970652E697356616C69644C6162656C3D66756E6374696F6E2874297B72657475726E20303C3D746869732E6C6162656C4D61702E696E6465784F662874297D2C742E70726F746F';
wwv_flow_api.g_varchar2_table(55) := '747970652E697356616C69643D66756E6374696F6E2874297B76617220653D7175696C6C2E6765745465787428742C31293B72657475726E2F5B412D5A235F5D2F2E746573742865297D2C742E70726F746F747970652E757064617465496E6465783D66';
wwv_flow_api.g_varchar2_table(56) := '756E6374696F6E28742C65297B72657475726E20742B312A657D2C742E70726F746F747970652E73657453656C656374696F6E3D66756E6374696F6E2874297B7175696C6C2E73657453656C656374696F6E28742E7374617274496E6465782C742E6C65';
wwv_flow_api.g_varchar2_table(57) := '6E6774682C2273696C656E7422297D2C742E70726F746F747970652E72656D6F766553656C656374696F6E3D66756E6374696F6E2874297B7175696C6C2E64656C6574655465787428742E7374617274496E6465782C742E6C656E677468297D2C747D28';
wwv_flow_api.g_varchar2_table(58) := '293B5175696C6C2E726567697374657228226D6F64756C65732F746578745F7265706C6163656D656E74222C546578745265706C6163656D656E74293B0A2F2F2320736F757263654D617070696E6755524C3D6170702E6D696E2E6A732E6D61700A';
null;
end;
/
begin
wwv_flow_api.create_plugin_file(
 p_id=>wwv_flow_api.id(81337200474172614)
,p_plugin_id=>wwv_flow_api.id(87902035304556045)
,p_file_name=>'plugin/app.min.js'
,p_mime_type=>'application/javascript'
,p_file_charset=>'utf-8'
,p_file_content=>wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table)
);
end;
/
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
wwv_flow_api.g_varchar2_table(1) := '2E656469746F722D636F756E7465727B77696474683A313030253B746578742D616C69676E3A72696768743B70616464696E672D72696768743A313070783B626F726465722D72696768743A2E35707820736F6C696420236335626662663B626F726465';
wwv_flow_api.g_varchar2_table(2) := '722D626F74746F6D3A2E35707820736F6C696420236335626662663B626F726465722D6C6566743A2E35707820736F6C696420236335626662663B626F726465722D746F703A2E33707820736F6C696420236335626662667D7370616E2E716C2D706963';
wwv_flow_api.g_varchar2_table(3) := '6B65722D6F7074696F6E733E2A7B6C696E652D6865696768743A31656D7D2E716C2D64726F70646F776E436F6E7461696E65722E716C2D7069636B65727B77696474683A2D7765626B69742D6669742D636F6E74656E743B77696474683A2D6D6F7A2D66';
wwv_flow_api.g_varchar2_table(4) := '69742D636F6E74656E743B77696474683A6669742D636F6E74656E743B6D696E2D77696474683A32303070783B6D617267696E3A30203570787D2E716C2D64726F70646F776E436F6E7461696E65722E716C2D7069636B6572202E716C2D7069636B6572';
wwv_flow_api.g_varchar2_table(5) := '2D6C6162656C3A3A6265666F72652C2E716C2D64726F70646F776E436F6E7461696E65722E716C2D7069636B65722E716C2D657870616E646564202E716C2D7069636B65722D6974656D3A3A6265666F72657B636F6E74656E743A617474722864617461';
wwv_flow_api.g_varchar2_table(6) := '2D636F6E74656E74297D2E716C2D736E6F77202E716C2D64726F70646F776E436F6E7461696E6572202E716C2D7069636B65722D6F7074696F6E73202E716C2D7069636B65722D6974656D3A3A6265666F72657B666F6E742D7765696768743A3530307D';
wwv_flow_api.g_varchar2_table(7) := '2E716C2D7069636B65722D6974656D7B706F736974696F6E3A72656C61746976657D2E716C2D736E6F772E716C2D746F6F6C626172202E716C2D7069636B65722D6974656D3A686F7665723A61667465727B636F6E74656E743A6174747228646174612D';
wwv_flow_api.g_varchar2_table(8) := '746F6F6C746970293B706F736974696F6E3A6162736F6C7574653B6261636B67726F756E643A233065306430643B6261636B67726F756E642D636F6C6F723A2330653064306463323B636F6C6F723A236635663566353B746F703A303B6C6566743A3130';
wwv_flow_api.g_varchar2_table(9) := '35253B70616464696E673A3570783B626F726465722D7261646975733A3670787D6469762E716C2D656469746F7220707B6D617267696E2D626F74746F6D3A313570787D0A2F2A2320736F757263654D617070696E6755524C3D6170702E6D696E2E6373';
wwv_flow_api.g_varchar2_table(10) := '732E6D6170202A2F0A';
null;
end;
/
begin
wwv_flow_api.create_plugin_file(
 p_id=>wwv_flow_api.id(81337626448174555)
,p_plugin_id=>wwv_flow_api.id(87902035304556045)
,p_file_name=>'plugin/app.min.css'
,p_mime_type=>'text/css'
,p_file_charset=>'utf-8'
,p_file_content=>wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table)
);
end;
/
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
wwv_flow_api.g_varchar2_table(1) := '766172205F637265617465436C617373203D2066756E6374696F6E202829207B0D0A2020202066756E6374696F6E20646566696E6550726F70657274696573287461726765742C2070726F707329207B0D0A2020202020202020666F7220287661722069';
wwv_flow_api.g_varchar2_table(2) := '203D20303B2069203C2070726F70732E6C656E6774683B20692B2B29207B0D0A2020202020202020202020207661722064657363726970746F72203D2070726F70735B695D3B0D0A20202020202020202020202064657363726970746F722E656E756D65';
wwv_flow_api.g_varchar2_table(3) := '7261626C65203D2064657363726970746F722E656E756D657261626C65207C7C2066616C73653B0D0A20202020202020202020202064657363726970746F722E636F6E666967757261626C65203D20747275653B0D0A2020202020202020202020206966';
wwv_flow_api.g_varchar2_table(4) := '20282276616C75652220696E2064657363726970746F72292064657363726970746F722E7772697461626C65203D20747275653B0D0A2020202020202020202020204F626A6563742E646566696E6550726F7065727479287461726765742C2064657363';
wwv_flow_api.g_varchar2_table(5) := '726970746F722E6B65792C2064657363726970746F72293B0D0A20202020202020207D0D0A202020207D0D0A2020202072657475726E2066756E6374696F6E2028436F6E7374727563746F722C2070726F746F50726F70732C2073746174696350726F70';
wwv_flow_api.g_varchar2_table(6) := '7329207B0D0A20202020202020206966202870726F746F50726F70732920646566696E6550726F7065727469657328436F6E7374727563746F722E70726F746F747970652C2070726F746F50726F7073293B0D0A20202020202020206966202873746174';
wwv_flow_api.g_varchar2_table(7) := '696350726F70732920646566696E6550726F7065727469657328436F6E7374727563746F722C2073746174696350726F7073293B0D0A202020202020202072657475726E20436F6E7374727563746F723B0D0A202020207D3B0D0A7D28293B0D0A0D0A66';
wwv_flow_api.g_varchar2_table(8) := '756E6374696F6E205F636C61737343616C6C436865636B28696E7374616E63652C20436F6E7374727563746F7229207B0D0A20202020696620282128696E7374616E636520696E7374616E63656F6620436F6E7374727563746F722929207B0D0A202020';
wwv_flow_api.g_varchar2_table(9) := '20202020207468726F77206E657720547970654572726F72282243616E6E6F742063616C6C206120636C61737320617320612066756E6374696F6E22293B0D0A202020207D0D0A7D0D0A0D0A66756E6374696F6E205F746F436F6E73756D61626C654172';
wwv_flow_api.g_varchar2_table(10) := '7261792861727229207B200D0A202020206966202841727261792E69734172726179286172722929207B200D0A2020202020202020666F7220287661722069203D20302C2061727232203D204172726179286172722E6C656E677468293B2069203C2061';
wwv_flow_api.g_varchar2_table(11) := '72722E6C656E6774683B20692B2B29207B200D0A202020202020202020202020617272325B695D203D206172725B695D3B200D0A20202020202020207D2072657475726E20617272323B200D0A202020207D20656C7365207B200D0A2020202020202020';
wwv_flow_api.g_varchar2_table(12) := '72657475726E2041727261792E66726F6D28617272293B200D0A202020207D200D0A7D';
null;
end;
/
begin
wwv_flow_api.create_plugin_file(
 p_id=>wwv_flow_api.id(81338722389178584)
,p_plugin_id=>wwv_flow_api.id(87902035304556045)
,p_file_name=>'plugin/ES6_Utility.js'
,p_mime_type=>'application/javascript'
,p_file_charset=>'utf-8'
,p_file_content=>wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table)
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false), p_is_component_import => true);
commit;
end;
/
set verify on feedback on define on
prompt  ...done
