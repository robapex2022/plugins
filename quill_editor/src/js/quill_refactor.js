// let quill = "";
// let Block = Quill.import("blots/block");        // REMOVE to return to <p><br></p> as default separator
// Block.tagName = "DIV";
// Quill.register(Block, true);
// let haveRemoved = false;
// let htmlContent = "";
// let htmlContainer = "";
// let textReplacements = "";
// let textTooltips = [];
// let bindings = null;

// function initialize(id, value, labelValues, labelToolTips, placeholder) {
//     initializeKeyBindings();                        // Create key bindings
//     initializeEventHandlers();                      // Create event handlers
//     initializeQuillEditor(id, value, labelValues, labelToolTips, placeholder);  // Setup editor with toolbars
//     onLoad();
// }

// function onLoad() {
//     quill.clipboard.dangerouslyPasteHTML(htmlContent);     // Populate the editor
//     quill.history.clear();                                 // Prevent an undo that removes all of the text
//     htmlContainer.val(htmlContent);                        // Populate the APEX item container
// };

// function initializeKeyBindings() {
//     // Key binding events
//      bindings = {
//         delete: {
//             key: 46,
//             handler: function (range, context) {
//                 let rightChar = quill.getText(range.index, 1);
//                 let format = quill.getFormat(range.index, 1);

//                 // Use default behavior if no BOLD # is found
//                 if (rightChar !== "#") { //} || !format.bold) {
//                     haveRemoved = true;
//                     return true;
//                 }
//             }
//         },
//         backspace: {
//             key: 8,
//             handler: function (range, context) {
//                 let leftChar = quill.getText(range.index - 1, 1);
//                 let format = quill.getFormat(range.index - 1, 1);

//                 // Use default behavior if no BOLD # is found
//                 if (leftChar !== "#") { //} || !format.bold) {
//                     haveRemoved = true;
//                     return true;
//                 }
//             }
//         },
//         paste: {
//             key: 86, //"v"
//             ctrlKey: true,
//             handler: function (range, context) {
//                 return true;
//             }
//         }
//     }
// };

// function initializeQuillEditor(id, value, labelValues, labelToolTips, placeholder) {
//     // Get the chosen toolbar options
//     // A checkbox provides a colon ":" delimited string of the options selected
//     let labelToolbar = ("' || l_label_dropdown || '" === "N") ? "" : [{ "dropdownContainer": [] }];
//     let toolbarGroup1 = "'|| l_toolbar_group_1 ||'".split(":");
//     let toolbarGroup2 = "'|| l_toolbar_group_2 ||'".split(":");
//     let labelMap = [];
//     let counter = ("' || l_word_limit || '" === "Y") ? {
//         container: ".editor-counter",
//         editor: ".ql-editor",
//         counter: "p.character-count",
//         unit: "character",
//         maxCount: ' || l_max_word_count || '
//     } : "";
//     // Handle null cases
//     toolbarGroup1 = toolbarGroup1[0] === "" ? "" : toolbarGroup1;
//     toolbarGroup2 = toolbarGroup2[0] === "" ? "" : toolbarGroup2;

//     // Create an array of toolbars
//     // let toolbars = [].concat(_toConsumableArray(labelToolbar), _toConsumableArray(toolbarGroup1), _toConsumableArray(toolbarGroup2));

//     let toolbars = [...labelToolbar, ...toolbarGroup1, ...toolbarGroup2];
//     /* ES6 Not compatible with IE
//         let toolbars = [...toolbarGroup1, ...toolbarGroup2];
//     */
//     let labelValues = { ' || l_label_values || '};

//     Object.keys(labelValues).forEach(function (key) {
//         key, labelValues[key]
//         textReplacements += (key) + ":";
//         textTooltips.push(labelValues[key]);
//     });

//     // Remove the trailing ":"
//     textReplacements = textReplacements.slice(0, -1);

//     // Create a label map, so that only labels specified will be highlighted
//     let textRepl2 = textReplacements.split(":");
//     for (let textReplacement of textRepl2) {
//         let numValue = 0;
//         for (let i = 0; i < textReplacement.length; i++) {
//             numValue += textReplacement.charCodeAt(i);
//         }
//         labelMap.push(numValue);
//     }

//     // Setup the quill editor
//     quill = new Quill(".editor-container", {
//         modules: {
//             keyboard: {
//                 bindings: bindings
//             },
//             clipboard: {
//                 matchVisual: false                        // Prevents quill from adding extra newlines
//             },
//             toolbar: [
//                 //[{ "dropdownContainer": [] }],            // Necessary placeholder dropdown that will be replaced (custom dropdowns are not supported)
//                 //[...toolbars],                            // Get all of the toolbars defined
//                 [].concat(_toConsumableArray(toolbars))     // Get all of the toolbars defined
//             ],
//             replacement_dropdown: {                       // Replacement Dropdown module that customizes the dropdown with our content
//                 container: ".editor-container",
//                 content: "'||'",
//                 textReplacementList: textReplacements,
//                 textTooltipList: textTooltips
//             },
//             text_replacement: {                           // Text Replacement Module treats text replacements as a single unit rather than
//                 delimiter: "#",                            // a series of characters
//                 labelMap: labelMap
//             },
//             counter
//         },
//         placeholder: "'|| l_placeholder || '",
//         theme: "snow"
//     });
//     htmlContainer = apex.jQuery("#'|| p_item.name ||'");                    // Get a hold of the html container
//     htmlContent = '|| '''' || TRIM('"' from                                        -- Workaround for es6 template literals 
//     REPLACE(--Get rid of leading / ending quotes
//                                                           REPLACE(
//             REPLACE(p_value, '''', '&#39'), --Get rid of apostrophes
//                                                           chr(10), ''), --Get rid of vertical tabs
//                                                       chr(13), '')-- Get rid of newline
//                                                   ) || '''' || ';                                      // Need to remove single quotes (workaround for ES6 template literals)    
//     console.log(htmlContent);
//     apex.jQuery(".ql-toolbar").css("backgroundColor", "#fff");               // Change the background color of the toolbar

//     htmlContainer.on("change", function () {
//         htmlContent = apex.item("' || p_item.name || '").getValue();
//         quill.clipboard.dangerouslyPasteHTML(htmlContent);     // Populate the editor
//         quill.history.clear();                                 // Prevent an undo that removes all of the text
//     });
// }

// function initializeEventHandlers() {
//     quill.on("text-change", function () {
//         let text = quill.root.innerHTML;                   // Get the innerHTML
//         htmlContainer.val(text);                           // Update the APEX item container with the edited content       
//     });
// }
